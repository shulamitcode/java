package com.shuli;

public interface QueueManager <K,V>{
	
	public <K,V> void add(K k,V v) ;
	public V  get(K k) ;

}
