package com.shuli;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

@WebServlet("/shuli")
public class HelloWorldServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<String> ll = new ArrayList<String>();
		Gson gson = new Gson();
		StringBuilder sb = new StringBuilder();
		String s;
		while ((s = req.getReader().readLine()) != null) {
			sb.append(s);
		}
		Student student = (Student) gson.fromJson(sb.toString(), Student.class);
		// resp.setContentType("text/plain");
		// resp.getWriter().write("Hello World! Maven Web Project Example.");

		String json = new Gson().toJson(student);
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(json);

	}

}
